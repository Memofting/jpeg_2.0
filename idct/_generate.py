import numpy as np
from math import cos, pi, sqrt
import pickle
import os


DEF_PATH = os.path.join('idct', 'matrix')


def write_matrix(file_name):
    C = np.array([1 / sqrt(2), 1, 1, 1, 1, 1, 1, 1])

    V = []
    for x in range(8):
        Cos = np.fromiter(map(cos, ((2*x + 1)*u*pi/16 for u in range(8))),
                          dtype='double')
        V.append(C * Cos)
        print("append: {}".format(x))

    M = np.zeros((8, 8, 64))
    for i in range(64):
        x, y = i // 8, i % 8
        M[x, y] = np.array([V[x][i] * V[y] for i in range(8)]).flatten()

    with open(file_name, 'bw') as fn:
        pickle.dump(0.25 * M, fn)


if __name__ == "__main__":
    write_matrix(DEF_PATH)
