import unittest
import os
from source import decoder
from .image_generating import (get_monotonus_rgb_images,
                               get_monotonus_grayscale_images,
                               strip_up_descriptor, strip_right_descriptor)
from source._exif_supprot import IFD1, thumbnail_image


class TestBaseline(unittest.TestCase):
    def test_monotonus_rgb_images(self):
        for full_name, image in get_monotonus_rgb_images():
            d = decoder.JpegDecoder(full_name)
            decoded_img = None
            for decoded_img in d.decode():
                pass
            for act, comp in zip((image[..., ::-1] / 255.0).flat,
                                 decoded_img.flat):
                self.assertAlmostEqual(act, comp, delta=1e-2)

    def test_monotonus_grayscale_images(self):
        for full_name, image in get_monotonus_grayscale_images():
            d = decoder.JpegDecoder(full_name)
            decoded_img = None
            for decoded_img in d.decode():
                pass
            for act, comp in zip((image / 255.0).flat, decoded_img.flat):
                self.assertAlmostEqual(act, comp, delta=1e-2)

    def test_stripped_images(self):
        for descriptor in (strip_up_descriptor, strip_right_descriptor):
            d = decoder.JpegDecoder(descriptor[0])
            image = None
            for image in d.decode():
                pass
            for act, comp in zip((descriptor[1] / 255.0).flat, image.flat):
                self.assertAlmostEqual(act, comp, delta=1e-2)

    def test_exif_thumbnail(self):
        img = decoder.JpegDecoder(os.path.join('temp', 'exif.jpg'))
        rgb_image = None
        for rgb_image in img.decode():
            pass

        img_with_exif = decoder.JpegDecoder(
            os.path.join('images', 'exif', 'real_image.jpg'))
        exif_img = img_with_exif.exif.get_from(IFD1, thumbnail_image)

        for act, comp in zip(rgb_image.flat, exif_img.flat):
            self.assertAlmostEqual(act, comp, delta=1e-2)
