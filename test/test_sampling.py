import unittest
from source.decoder import JpegDecoder
import os
import numpy as np
from functools import partial


get_file_path = partial(os.path.join, os.path.join(os.getcwd(),
                        "images", "modified_sampling_factor"))


class TestBaseline(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        rgb = None
        for rgb in JpegDecoder(get_file_path("rgb.jpg")).decode():
            pass

        gray = None
        for gray in JpegDecoder(get_file_path("gray.jpg")).decode():
            pass

        cls.rgb = rgb
        cls.gray = gray

    def test_vertical_gray_spacing(self):
        d_test = JpegDecoder(get_file_path("gray_vertical.jpg"))
        img_test = self._get_image(d_test)

        self._assert_images(self.gray, img_test)

    def test_horizontal_gray_spacing(self):
        d_test = JpegDecoder(get_file_path("gray_horizontal.jpg"))
        img_test = self._get_image(d_test)

        self._assert_images(self.gray, img_test)

    def test_vertical_rgb_spacing(self):
        d_test = JpegDecoder(get_file_path("rgb_vertical.jpg"))
        img_test = self._get_image(d_test)

        self._assert_images(self.rgb, img_test)

    def test_horizontal_rgb_spacing(self):
        d_test = JpegDecoder(get_file_path("rgb_horizontal.jpg"))
        img_test = self._get_image(d_test)

        self._assert_images(self.rgb, img_test)

    def _get_image(self, decoder):
        img_test = None
        for img_test in decoder.decode():
            pass

        return img_test

    def _assert_images(self, expected, actual):
        self.assertTrue(expected.shape == actual.shape, "shaped differ")
        self.assertAlmostEqual(np.sum(expected - actual) / expected.size,
                               0, delta=1e-3, msg="images differ")
