import numpy as np
import cv2
import os
import functools


rgb, grayscale = 'rgb', 'grayscale'
im_dir = os.path.join("images")

colors = {
    rgb: {
        "blue": np.array([255, 0, 0]),
        "green": np.array([0, 255, 0]),
        "red": np.array([0, 0, 255]),
        "gray": np.array([127, 127, 127]),
        "black": np.array([0, 0, 0]),
        "white": np.array([255, 255, 255]),
    },
    grayscale: {
        "white": 255,
        "gray": 128,
        "black": 0,
    }
}

shapes = {
    rgb: [
        (8, 8, 3),
        (16, 16, 3),
        (32, 32, 3),
        (64, 64, 3),
        (128, 128, 3),
        (256, 256, 3),
        (1024, 720, 3)
    ],
    grayscale: [
        (8, 8),
        (16, 16),
        (32, 32),
        (64, 64),
        (128, 128),
        (256, 128),
        (256, 256),
    ]
}

_strip_right_ndarr = np.zeros((8, 8), dtype='uint8')
_strip_right_ndarr[4:6, :] = 255
strip_right_descriptor = (os.path.join(im_dir, grayscale, "strip_right.jpg"),
                          _strip_right_ndarr)

_strip_up_ndarr = 255 * np.ones((8, 8), dtype='uint8')
_strip_up_ndarr[:, 4:6] = 0
strip_up_descriptor = (os.path.join(im_dir, grayscale, "strip_up.jpg"),
                       _strip_up_ndarr)


def get_monotonus_rgb_images():
    yield from _get_monotonus_images(modes=[rgb])


def get_monotonus_grayscale_images():
    yield from _get_monotonus_images(modes=[grayscale])


def _get_monotonus_images(modes=[rgb, grayscale]):
    for mode in modes:
        os.makedirs(os.path.join(im_dir, mode), exist_ok=True)
        pattern_arrays = list(map(functools.partial(np.ones, dtype='uint8'),
                                  shapes[mode]))
        for color_name, color_repr in colors[mode].items():
            for pat_arr in pattern_arrays:
                img = pat_arr * color_repr
                img_name = "{}__{}.jpg".format(
                    color_name, "_".join(map(str, pat_arr.shape)))
                full_name = os.path.join(im_dir, mode, img_name)
                yield os.path.abspath(full_name), img


def main():
    for full_name, img in get_monotonus_rgb_images():
        cv2.imwrite(full_name, img)
    for full_name, img in get_monotonus_grayscale_images():
        cv2.imwrite(full_name, img)
    cv2.imwrite(*strip_right_descriptor)
    cv2.imwrite(*strip_up_descriptor)


if __name__ == "__main__":
    main()
