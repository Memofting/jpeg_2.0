import argparse
import os
import sys

from source import decoder
from source._exif import IFD1, thumbnail_image
from gui.visual_mode import visual_mode
import pprint

pp = pprint.PrettyPrinter(indent=4)


def main():
    parser = argparse.ArgumentParser(
        usage='{} [OPTIONS] FILE'.format(os.path.basename(sys.argv[0])),
        description="Jpeg descriptor. It can work only either in "
                    "console or visual mode in one time. "
                    "As default it has setted up to console mode")
    parser.add_argument('fn', metavar='FILE',
                        help='name of a image')
    parser.add_argument('-v', '--visual', action='store_true', dest='visual',
                        help='Enter in visual mode '
                             '(it requires installed PyQt5).')
    parser.add_argument('-e', '--exif', action='store_true', dest='exif',
                        help="Show exif information from jpeg (Console)."
                             "Show thumbnail image from exif, if it "
                             "encoded with jpeg (Visual)")
    parser.add_argument('-c', '--channel', action='store_true', dest='channel',
                        help="Print image as 3 dimension vector (RGB). "
                             "For grayscale image prints only one "
                             "dimension (Console). - (Visual)")
    parser.add_argument('-d', '--detailed', action='store_true',
                        dest='detailed',
                        help='Print detailed information about '
                             'file parsing')
    args = parser.parse_args()
    d = decoder.JpegDecoder(args.fn, detailed=args.detailed)
    iterable_for_update, img_name = None, None

    if args.exif:
        if not d.exif:
            sys.exit("File doesn't contain any exif data frames")
        if not args.visual:
            pp.pprint(d.exif)
        elif d.exif.has_thumbnail():
            image = d.exif.get_from(IFD1, thumbnail_image)
            iterable_for_update = [image]
            img_name = "Thumbnail image"
    elif args.visual:
        iterable_for_update = d.decode()
        img_name = args.fn
    else:
        args.channel = True

    if iterable_for_update is not None:
        visual_mode(iterable_for_update, img_name)
    if args.channel:
        image = None
        for image in d.decode():
            pass
        mode = "RGB" if len(image.shape) == 3 else "GrayScale"
        print("Mode: {}".format(mode))
        if mode == "RGB":
            for i, name in enumerate(mode):
                print("{}: {}".format(name, image[:, :, i].flatten()))
        else:
            print(image.flatten())


if __name__ == "__main__":
    main()
