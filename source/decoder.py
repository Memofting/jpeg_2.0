import sys
import numpy as np
import io
import os
from collections import OrderedDict
from collections import defaultdict
from ._bytes import i8, i16, get_bits_flow, get_component
from ._functions import idct, YCrCb2RGB, Y2Grayscale
from ._markers import MARKER


class JpegDecoder:
    def __init__(self, image_name, buf=None, detailed=False):
        self.exif = None
        self.comment = None
        self.is_baseline = True
        self.image_name = image_name
        self.data_start = 0
        self.data_end = 0
        self.h, self.w = 0, 0

        self.dqt_tables = {}
        self.scans_attr = {}
        self.scan_sels = []
        self.scan_tables = OrderedDict()
        self.huf_tables = {}

        self.fp = None
        if image_name is None:
            self.fp = io.BytesIO(buf)
        else:
            if not os.path.exists(image_name):
                sys.exit("Image: '{}' not found".format(image_name))
            self.fp = open(image_name, 'rb')

        s = self.fp.read(1)
        if i8(s) != 255:
            sys.exit("Image is not a JPEG")

        try:
            while True:
                i = i8(s)
                if i == 0xFF:
                    s = s + self.fp.read(1)
                    i = i16(s)
                if i in MARKER:
                    name, description, handler = MARKER[i]
                    if detailed:
                        print(name, description)
                    if handler is not None:
                        handler(self, self.fp)
                    s = self.fp.read(1)
                    if i == 0xFFDA:
                        break
                elif i == 0 or i == 0xFFFF:
                    # padded marker or junk; move on
                    s = b"\xff"
                elif i == 0xFF00:
                    # Skip extraneous data (escaped 0xFF)
                    s = self.fp.read(1)
                else:
                    # encoded image data frame
                    s = self.fp.read(1)
        except KeyError:
            sys.exit("Unrecognixed marker")
        except EOFError:
            pass

        self.fp.seek(0)
        self.image_wb = np.ndarray((self.h, self.w))
        self.image_rgb = np.ndarray((self.h, self.w, 3))
        """
        self.scan_tables:
            {scan_sel: [ScanTable+]}
        """
        if self.is_baseline:
            for tables in self.scan_tables.values():
                for table in tables.scan_tables:
                    table.try_add_huf_tables(self.huf_tables)
        else:
            raise Exception("unsupported mode: progressive")

        for scan_sel, attr in self.scans_attr.items():
            self.scan_tables[scan_sel].add_attr(attr)

        if detailed:
            self.print_detailed()

    def print_detailed(self):
        if self.comment is not None:
            print("\nComment:\n{}".format(self.comment))
        print("\nScan tables:")
        for table in self.scan_tables.values():
            print(table)
        print("\nDequantinezation tables:")
        for k, w in self.dqt_tables.items():
            print("{}:\n{}".format(k, w))

    def decode(self):
        """
        decode initialized image and yield image with decoded scans
        """
        image = None
        is_set_image = False
        self.fp.seek(self.data_start)
        bits_flow = get_bits_flow(self.fp.read())
        dc_shifts = dict(zip(self.scan_tables.keys(),
                             (0 for _ in self.scan_tables.keys())))

        huf_tables = {}
        shapes = {}
        for scan_sel, scan_tables in self.scan_tables.items():
            cur_shapes = []
            for _ in range(scan_tables.attr.v_samp * scan_tables.attr.h_samp):
                new_shape = np.zeros((64, ), dtype='int')
                cur_shapes.append(new_shape)
            shapes[scan_sel] = cur_shapes

        y, x = 0, 0
        while True:
            scans_idct = defaultdict(list)
            for scan_sel, scan_tables in self.scan_tables.items():
                for arr in shapes[scan_sel]:
                    for scan_table in scan_tables.scan_tables:
                        huf_tables.update(scan_table.huf_tables)
                        dc_id, ac_id = scan_table.dc_id, scan_table.ac_id
                        dc_table = huf_tables.get((0, dc_id))
                        ac_table = huf_tables.get((1, ac_id))
                        arr = arr * 0
                        arr[0] = dc_shifts[scan_sel]
                        scan, dc_shift = get_component(bits_flow, dc_table,
                                                       ac_table, arr,
                                                       scan_table,
                                                       scan_tables)
                        if dc_shift is not None:
                            dc_shifts[scan_sel] = dc_shift
                        dqt_table = self.dqt_tables[scan_tables.attr.dqt_table]
                        scan_idct = idct(scan * dqt_table)
                        scans_idct[scan_sel].append(scan_idct)

            Yn = scans_idct.get(1, None)
            Cb = scans_idct.get(2, [None])[-1]
            Cr = scans_idct.get(3, [None])[-1]

            if not is_set_image:
                if Cr is None or Cb is None:
                    image = self.image_wb
                else:
                    image = self.image_rgb
                is_set_image = True

            # ужас и страх
            x_side = 8 * self.scans_attr[1].h_samp
            y_side = 8 * self.scans_attr[1].v_samp

            if Cr is None or Cb is None:
                x_side, y_side = max(x_side, y_side), min(x_side, y_side)
                Y2Grayscale(y, self.h, x, self.w, image,
                            Yn, x_side, y_side)

            else:
                YCrCb2RGB(y, self.h, x, self.w, image,
                          Yn, Cb, Cr, x_side, y_side)
            yield image
            x = x + x_side
            if x >= self.w:
                x = 0
                y = y + y_side
                if y >= self.h:
                    break

    def __del__(self):
        if self.fp:
            self.fp.close()
