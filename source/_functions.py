import numpy as np
import itertools
import pickle
from idct._generate import DEF_PATH


with open(DEF_PATH, 'br') as fn:
    M = np.array(pickle.load(fn)).reshape((8, 8, 64))


def idct(array_2d):
    """
    compute idct for matrix 8x8
    """
    return np.dot(M, array_2d.flatten())


def matr_norm_thres(matrix, l_thres=0, h_thres=255):
    matrix[matrix < l_thres] = l_thres
    matrix[matrix > h_thres] = h_thres
    return matrix / (h_thres - l_thres)


def Y2Grayscale(y, h, x, w, out, Yn: list, x_side, y_side):
    form = (y_side, x_side)

    Y = stack_y(form, Yn)
    thres_matr = matr_norm_thres(128 + Y)

    shape = (min(h-y, y_side), min(w-x, x_side))
    res = fit_in_shape(shape, thres_matr)
    out[y:y+y_side, x:x+x_side] = res
    pass


def YCrCb2RGB(y, h, x, w, out, Yn: list, Cb, Cr, x_side, y_side):
    form = (y_side, x_side)

    Y = stack_y(form, Yn)
    Cr = expand(form, Cr)
    Cb = expand(form, Cb)

    shape = (min(h-y, y_side), min(w-x, x_side))

    R = matr_norm_thres(128 + Y + 1.402 * Cr)
    G = matr_norm_thres(128 + Y - 0.34414 * Cb - 0.71414 * Cr)
    B = matr_norm_thres(128 + Y + 1.772 * Cb)

    R = fit_in_shape(shape, R)
    G = fit_in_shape(shape, G)
    B = fit_in_shape(shape, B)

    out[y:y+y_side, x:x+x_side, 0] = R
    out[y:y+y_side, x:x+x_side, 1] = G
    out[y:y+y_side, x:x+x_side, 2] = B


def fit_in_shape(shape, pat):
    if pat.shape != shape:
        return pat[:shape[0], :shape[1]]
    return pat


def stack_y(shape, args):
    x_side, y_side = shape
    len_args = len(args)
    if len_args == 4:
        a, b, c, d = args
        return np.vstack((np.hstack((a, b)), np.hstack((c, d))))
    elif len_args == 2:
        if x_side > y_side:
            return _stack_v(*args)
        else:
            return _stack_h(*args)
    return args[0]


def _stack_v(a, b):
    return np.vstack((a, b))


def _stack_h(a, b):
    return np.hstack((a, b))


def expand(shape, matrix):
    res = np.ndarray(shape)
    tf_x = shape[0] // 8
    tf_y = shape[1] // 8
    for i in range(64):
        x, y = i // 8, i % 8
        res[x*tf_x:(x+1)*tf_x, y*tf_y:(y+1)*tf_y] = matrix[x, y]
    return res


def repeat_each(iterable, n=4):
    """
    make iterator where each element is corresponding of iterable
    with n repeating.
    """
    return itertools.chain.from_iterable(zip(*itertools.tee(iterable, n)))
