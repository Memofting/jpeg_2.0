import numpy as np
from struct import unpack_from, unpack
from itertools import accumulate, islice


zigzag_order = [
    [0, 1, 5, 6, 14, 15, 27, 28],
    [2, 4, 7, 13, 16, 26, 29, 42],
    [3, 8, 12, 17, 25, 30, 41, 43],
    [9, 11, 18, 24, 31, 40, 44, 53],
    [10, 19, 23, 32, 39, 45, 52, 54],
    [20, 22, 33, 38, 46, 51, 55, 60],
    [21, 34, 37, 47, 50, 56, 59, 61],
    [35, 36, 48, 49, 57, 58, 62, 63]
]


flat_zigzag = np.array(zigzag_order).reshape((64,))


def i8(c):
    return c if c.__class__ is int else unpack("B", c)[0]


def i16(c, o=0):
    """
    Converts a 2-bytes (16 bits) string to an unsigned integer.
    c: string containing bytes to convert
    o: offset of bytes to convert in string
    """
    return unpack_from(">H", c, o)[0]


def get_bits_flow(buf):
    prev = -1
    for b in buf:
        if prev == 255 and b == 0:
            prev = b
            continue
        prev = b
        yield from _bit_repr(b)
    raise EOFError


_pow_2 = list(map(lambda x: 2 ** x, range(7, -1, -1)))


def _bit_repr(b):
    """
    return iterator through bits in binary representation of byte
    """
    for i in _pow_2:
        div = b // i
        if div:
            b -= i
        yield div


def get_component(bits_flow, dc_huf_table, ac_huf_table, scan,
                  scan_info, eobrun_cont):
    ss, se = scan_info.ss, scan_info.se
    if ss == 0 and se == 63:
        # baseline without succesive approximation
        return get_base_component(bits_flow, dc_huf_table, ac_huf_table,
                                  scan), scan[0]
    else:
        # progressive
        pass


def get_base_component(bits_flow, dc_huf_table, ac_huf_table, scan):
    scan[0] = scan[0] + _decode_dc(bits_flow, dc_huf_table)
    _decode_ac(bits_flow, ac_huf_table, scan)
    return scan[flat_zigzag].reshape((8, 8))


def _decode_ac(bits_flow, huf_table, array):
    k = 1
    while k < 64:
        rs = _decode(bits_flow, huf_table)
        low_bits = rs % 16
        hight_bits = rs >> 4
        r = hight_bits
        if low_bits == 0:
            if r == 15:
                k += 16
                continue
            break
        else:
            k += r
            zz = _receive(bits_flow, low_bits)
            zz = _extend(zz, low_bits)
            array[k] = zz
        k += 1


def _decode_dc(bits_flow, huf_table):
    rs = _decode(bits_flow, huf_table)
    if rs == 0:
        return 0
    diff = _receive(bits_flow, rs)
    diff = _extend(diff, rs)
    return diff


def _decode(bits_flow, huf_table):
    length = 1
    t = None
    for b in bits_flow:
        if t is None:
            t = b
        else:
            t = (t << 1) + b
        code = huf_table.codes_len[length].get(t)
        if code is not None:
            return code
        length += 1


def _receive(bits_flow, length):
    val = None
    for val in islice(accumulate(bits_flow,
                      lambda res, x: res * 2 + x), length):
        pass
    return val


def _extend(val, length):
    val_len = 1 << (length - 1)
    try:
        if val < val_len:
            val = val - (val_len << 1) + 1
        return val
    except TypeError:
        raise
