class HufTable:
    def __init__(self):
        self.codes_len = {}
        for i in range(1, 17):
            self.codes_len.setdefault(i, {})

    def add_val(self, l, code, val):
        self.codes_len[l].update(((code, val),))

    def get_val(self, l, code):
        try:
            self.codes_len[l].get(code)
        except KeyError:
            print(l, code)
            print(self.codes_len)
            raise

    def __str__(self):
        buf = []
        for length, codes in self.codes_len.items():
            buf.append("length: {}. codes: {}".format(length,
                                                      list(codes.values())))
        return "\n".join(buf)

    def __repr__(self):
        return self.__str__()
