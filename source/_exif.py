from ._bytes import i16
from struct import unpack
from source import decoder
from fractions import Fraction
from ._exif_supprot import *


def define_dataformat(byte_order=">"):
    return {
        1: ("unsigned byte",
            lambda l, x: unpack("B", x), 1),
        2: ("ascii string",
            lambda l, x: unpack("{}s".format(l), x), 1),
        3: ("unsigned short",
            lambda l, x: unpack("{}H".format(byte_order), x), 2),
        4: ("unsigned long",
            lambda l, x: unpack("{}L".format(byte_order), x), 4),
        5: ("unsigned rational",
            lambda l, x: [Fraction(*unpack("{}2L".format(byte_order), x))], 8),
        6: ("signed byte",
            lambda l, x: unpack("b", x), 1),
        7: ("undefined",
            lambda l, x: unpack("c", x), 1),
        8: ("signed short",
            lambda l, x: unpack("{}h".format(byte_order), x), 2),
        9: ("signed long",
            lambda l, x: unpack("{}l".format(byte_order), x), 4),
        10: ("signed rational",
             lambda l, x: [Fraction(*unpack("{}2l".format(byte_order),
                                    x))], 8),
        11: ("single float",
             lambda l, x: unpack("{}f".format(byte_order), x), 4),
        12: ("double float",
             lambda l, x: unpack("{}d".format(byte_order), x), 8),
    }


# little endian
DATAFORMAT_II = define_dataformat("<")
# big endian
DATAFORMAT_MM = define_dataformat(">")


def APP(self, fp):
    length = i16(fp.read(2)) - 2
    end = fp.tell() + length
    # parse header
    name = fp.read(6)
    if name[:4] != b'Exif':
        return
    start = fp.tell()
    bo = ">"
    DATAFORMAT = DATAFORMAT_MM
    if fp.read(2) == b'II':
        bo = "<"
        DATAFORMAT = DATAFORMAT_II
    if unpack("{}h".format(bo), fp.read(2))[0] != b'\x2A'[0]:
        return
    ifd_offset = unpack("{}I".format(bo), fp.read(4))[0]
    fp.seek(start + ifd_offset)

    additional_offsets = []
    exif_data = ExifData()
    # image file directory
    ifd_name = IFD0
    while True:
        number = unpack("{}h".format(bo), fp.read(2))[0]
        for _ in range(number):
            tag = fp.read(2)
            f = unpack("{}h".format(bo), fp.read(2))[0]
            name, df, byte_size = DATAFORMAT[f]
            dn = unpack("{}i".format(bo), fp.read(4))[0]
            dl = dn * byte_size
            d = []
            if dl <= 4:
                dd = fp.read(4)
                s = 0
                for e in range(byte_size, dl+1, byte_size):
                    d.append((df(1, dd[s:e])[0], dl, name))
                    s = e
            else:
                offset = unpack("{}i".format(bo), fp.read(4))[0]
                cur = fp.tell()
                fp.seek(start + offset, 0)
                data = fp.read(dl)
                d.append((df(dn, data)[0], dl, name))
                fp.seek(cur, 0)
            if tag == b'\x87\x69':
                additional_offsets.append(d[0])
            exif_data.add_to(ifd_name, tag, d)

        compression = exif_data.get_from(ifd_name, "Compression")
        if compression and compression == [(6, 2, "unsigned short")]:
            jpeg_offset = exif_data.get_from(ifd_name, "JpegIFOffset")[0][0]
            jpeg_size = exif_data.get_from(ifd_name, "JpegIFByteCount")[0][0]
            cur = fp.tell()

            fp.seek(start + jpeg_offset)
            dec = decoder.JpegDecoder(None, fp.read(jpeg_size))
            fp.seek(cur)

            image = dec.decode()
            for image in dec.decode():
                pass
            exif_data.add_to(ifd_name, thumbnail_image, image)
        offset2next = unpack("{}I".format(bo), fp.read(4))[0]
        if offset2next == 0:
            ifd_name = SubIFD
            try:
                offset2next = additional_offsets.pop()[0]
            except IndexError:
                break
        else:
            ifd_name = IFD1
        fp.seek(start + offset2next)

    fp.seek(end)
    self.exif = exif_data


def bytes2tagname(directory):
    res = {}
    for k, w in directory.items():
        new_name = TAGS.get(k)
        if new_name:
            # because not all tag's names are listed in EXIF_TAGS
            res[new_name] = w
    return res
