class ScanTable:
    def __init__(self, scan_sel, dc_id, ac_id):
        self.scan_sel = scan_sel
        self.dc_id = dc_id
        self.ac_id = ac_id
        self.huf_tables = {}
        self.ss = None
        self.se = None
        self.ah = None
        self.al = None

    def update_prog(self, ss, se, ah, al):
        self.ss = ss
        self.se = se
        self.ah = ah
        self.al = al

    def try_add_huf_tables(self, huf_tables):
        is_success = False
        for (t_class, t_id), huf_table in huf_tables.items():
            if ((t_class, t_id) not in self.huf_tables):
                if (t_class == 0 and self.dc_id == t_id
                   or t_class == 1 and self.ac_id == t_id):
                    if huf_table:
                        self.huf_tables.update({(t_class, t_id): huf_table[0]})
                        is_success = True
        return is_success

    def __str__(self):
        huf_tables_description = []
        for name, val in self.huf_tables.items():
            huf_tables_description.append(
                "\n(class, id): ({name[0]}, {name[1]})\n{val}".format(
                    name=name, val=val))
        res = "".join((
            "\n",
            "Cn: {}, Dc: {}, Ac: {}.\n".format(
                    self.scan_sel, self.dc_id, self.ac_id),
            "ss: {}, se: {}, ah: {}, al: {}.\n".format(
                self.ss, self.se, self.ah, self.al),
            "HufTables:{}".format(
                '\n'.join(huf_tables_description))))
        return res

    def __repr__(self):
        return self.__str__()


class ScanAttr:
    def __init__(self, scan_sel, h_samp, v_samp, dqt_table):
        self.scan_sel = scan_sel
        self.h_samp = h_samp
        self.v_samp = v_samp
        self.dqt_table = dqt_table

    def get_ration(self):
        return self.h_samp, self.v_samp

    def __str__(self):
        return "Cn: {}, Hs: {}, Vs: {}, Di: {}".format(
            self.scan_sel, self.h_samp, self.v_samp, self.dqt_table)


class ScanTables:
    def __init__(self):
        self.scan_tables = []
        self.attr = None
        self.eobrun = 0

    def add_scan_table(self, table):
        self.scan_tables.append(table)

    def add_attr(self, attr):
        self.attr = attr

    def __repr__(self):
        body = (
            "ScanAttr:",
            str(self.attr),
            "\n",
            "ScanTables:",
            "\n".join(map(str, self.scan_tables)),
            "\n"
        )

        return "\n".join(body)
