import numpy as np
from ._bytes import i8, i16, flat_zigzag
from ._huf_table import HufTable
from ._exif import APP
from .ScanTables import ScanTable, ScanAttr, ScanTables
from collections import deque


# DHT Define Huffman Tables
# DAC Define Arithmetic Conditioning
# DQT Define Quantization Tables
# DRI Define Restart Interval
# APP n Application defined marker
# COM Comment


def Skip(self, fp):
    length = i16(fp.read(2)) - 2
    fp.seek(length, 1)


def COM(self, fp):
    length = i16(fp.read(2)) - 2

    data = fp.read(length).decode(encoding='ascii')
    self.comment = data


def SOF(self, fp):
    length = i16(fp.read(2)) - 2
    # if precision is 12 then this is extended mode
    precision = i8(fp.read(1))
    # Value 0 indicates that the number of lines shall be defined by the DNL
    # marker and parameters at the end of the first scan
    self.h, self.w = i16(fp.read(2)), i16(fp.read(2))
    num_comp = i8(fp.read(1))
    for _ in range(num_comp):
        scan_sel = i8(fp.read(1))
        b = i8(fp.read(1))
        h_samp, v_samp = b >> 4, b & 15
        dqt_table_id = i8(fp.read(1))
        self.scans_attr[scan_sel] = ScanAttr(scan_sel, h_samp,
                                             v_samp, dqt_table_id)


def DQT(self, fp):
    length = i16(fp.read(2)) - 2
    end = fp.tell() + length
    while fp.tell() < end:
        flat_table = np.zeros((64,))
        info = i8(fp.read(1))
        prec, table_id = info // 16, info & 15
        reader = i8
        if prec == 1:
            reader = i16
        for i in range(64):
            flat_table[i] = reader(fp.read(prec + 1))
        table = flat_table[flat_zigzag].reshape((8, 8))
        self.dqt_tables[table_id] = table


def DHT(self, fp):
    length = i16(fp.read(2)) - 2
    end = fp.tell() + length
    while fp.tell() < end:
        b = i8(fp.read(1))
        table_class, table_id = b >> 4, b & 15
        amount_symbols = []
        for _ in range(16):
            amount_symbols.append(i8(fp.read(1)))
        code = 0
        huf_table = HufTable()
        for i in range(16):
            for size in range(amount_symbols[i]):
                val = i8(fp.read(1))
                huf_table.add_val(i+1, code, val)
                code += 1
            code <<= 1
        self.huf_tables.setdefault((table_class, table_id), deque())
        self.huf_tables[(table_class, table_id)].append(huf_table)


def EOI(self, fp):
    self.data_end = fp.tell() - 2
    raise EOFError


def SOS(self, fp):
    length = i16(fp.read(2)) - 2
    num_scans = i8(fp.read(1))
    scan_tables = {}
    for_del = set()
    for i in range(num_scans):
        scan_sel = i8(fp.read(1))
        b = i8(fp.read(1))
        dc_id, ac_id = b >> 4, b & 15
        for_del.add((0, dc_id))
        for_del.add((1, ac_id))
        table = ScanTable(scan_sel, dc_id, ac_id)
        table.try_add_huf_tables(self.huf_tables)
        scan_tables[scan_sel] = table
    for name, huf_table in self.huf_tables.items():
        if name in for_del and huf_table:
            huf_table.popleft()

    Ss = i8(fp.read(1))
    Se = i8(fp.read(1))
    b = i8(fp.read(1))
    Ah, Al = b >> 4, b & 15
    for scan_table in scan_tables.values():
        scan_table.update_prog(Ss, Se, Ah, Al)

    for scan_sel, scan_table in scan_tables.items():
        self.scan_tables.setdefault(scan_sel, ScanTables())
        self.scan_tables[scan_sel].add_scan_table(scan_table)

    self.data_start = fp.tell()


def SOF2(self, fp):
    self.is_baseline = False
    SOF(self, fp)


MARKER = {
    0xFFC0: ("SOF0", "Baseline DCT", SOF),
    # 0xFFC1: ("SOF1", "Extended Sequential DCT", SOF),
    0xFFC2: ("SOF2", "Progressive DCT", SOF2),
    # 0xFFC3: ("SOF3", "Spatial lossless", SOF),
    0xFFC4: ("DHT", "Define Huffman table", DHT),
    0xFFCC: ("DAC", "Define arithmetic coding conditioning", Skip),
    0xFFD8: ("SOI", "Start of image", None),
    0xFFD9: ("EOI", "End of image", EOI),
    0xFFDA: ("SOS", "Start of scan", SOS),
    0xFFDB: ("DQT", "Define quantization table", DQT),
    0xFFDC: ("DNL", "Define number of lines", Skip),
    # EXIF
    0xFFE1: ("APP1", "Application segment 1", APP),
    0xFFFE: ("COM", "Comment", COM),
}
