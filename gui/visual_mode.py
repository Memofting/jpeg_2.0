import threading
import sys
from collections import deque
from gui.MainWindow import MainWindow
from PyQt5 import QtWidgets


event_queue = deque()
lock = threading.Lock()


def timer_handler():
    with lock:
        if len(event_queue) > 0:
            threading.Thread(target=event_queue.pop()).start()
            event_queue.clear()
    # чтобы таймер не зависал на дорогих процедурах
    timer = threading.Timer(1, timer_handler)
    # to stop at shutdown
    timer.setDaemon(True)
    timer.start()


def visual_mode(iterable_for_update, *args):
    app = QtWidgets.QApplication([])
    window = MainWindow()
    threading.Thread(target=updater,
                     args=(window.update_image,
                           iterable_for_update, *args)).start()
    timer_handler()
    sys.exit(app.exec_())


def updater(handler, iterable_for_update, *args):
    for part in iterable_for_update:
        with lock:
            # иначе скапливается очень
            # много запросов, которые замедляют работу
            event_queue.clear()
            event_queue.append(lambda: handler(part, *args))
