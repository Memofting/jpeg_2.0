from PyQt5 import QtWidgets, QtGui, QtCore
import qimage2ndarray
from .BackShadow import BackShadow


def scale_image(size: tuple, image):
    """
    scale image to fit in the size
    """
    w, h = size
    if (image.size().width() > w or image.size().height() > h):
        aspect_ratio_mode = QtCore.Qt.KeepAspectRatio
        transform_mode = QtCore.Qt.SmoothTransformation
        return image.scaled(w, h, aspectRatioMode=aspect_ratio_mode,
                            transformMode=transform_mode)
    return image


class ScaledImageWidget(QtWidgets.QLabel):
    MIN_SIZE_PRODUCT = 10e2
    MAX_SIZE_PRODUCT = 10e6

    def __init__(self, parent, init_size):
        super().__init__(parent)
        self.init_size = init_size
        self.setFrameStyle(QtWidgets.QFrame.Box
                           | QtWidgets.QFrame.Panel)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.setSizePolicy(
            QtWidgets.QSizePolicy.Minimum,
            QtWidgets.QSizePolicy.Minimum
        )
        self.initialized = False
        self.setGraphicsEffect(BackShadow())

    def reinit(self, ndarr, image_name):
        image = scale_image(self.init_size,
                            qimage2ndarray.array2qimage(ndarr * 255))
        pixmap = QtGui.QPixmap.fromImage(image)
        if image_name and pixmap:
            self.initialized = True
            self.setPixmap(pixmap)
            self.original_image = image
            self.aspect_ratio = (
                    self.original_image.width()
                    / self.original_image.height())

    def wheelEvent(self, a0: QtGui.QWheelEvent):
        if not self.initialized:
            return
        pixmap = self.pixmap()
        delta = a0.angleDelta().y() // 3
        width = pixmap.width() + delta
        height = width / self.aspect_ratio
        if self.MIN_SIZE_PRODUCT < abs(width*height) < self.MAX_SIZE_PRODUCT:
            pixmap = QtGui.QPixmap(
                self.original_image.scaled(
                    width, round(height),
                    transformMode=QtCore.Qt.SmoothTransformation))
            self.setPixmap(pixmap)
