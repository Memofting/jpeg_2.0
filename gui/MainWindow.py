from PyQt5 import QtWidgets, QtGui, QtCore
from .ScaledImageWidget import ScaledImageWidget
import os


class Image(QtWidgets.QWidget):
    def __init__(self, parent, init_size):
        super().__init__(parent)
        self.name = None
        image_label = ScaledImageWidget(self, init_size)
        name_label = QtWidgets.QLabel()
        name_label.setAlignment(QtCore.Qt.AlignCenter)

        image_layout = QtWidgets.QHBoxLayout()
        image_layout.addStretch()
        image_layout.addWidget(image_label)
        image_layout.addStretch()

        layout = QtWidgets.QVBoxLayout()
        layout.addStretch()
        layout.addLayout(image_layout)
        layout.addWidget(name_label)
        layout.addStretch()
        self.setLayout(layout)

    def update_image(self, ndarr, image_name=None):
        layout = self.layout()
        image_label = layout.itemAt(1).layout().itemAt(1)
        name_label = layout.itemAt(2)
        if image_name is not None:
            self.name = image_name
            name_label.widget().setText(self.cut_name(image_name))
        image_label.widget().reinit(ndarr, self.name)

    @staticmethod
    def cut_name(image_name, defualt_suffix="...", width=20):
        name = os.path.split(image_name)[-1]
        if len(name) > width:
            name = name[:width - len(defualt_suffix)] + defualt_suffix
        return name


def empty_func(*args, **kwargs):
    pass


class MainWindow(QtWidgets.QMainWindow):
    im_size = (640, 480)
    wn_size = (720, 600)

    def __init__(self):
        super().__init__()
        scroll = QtWidgets.QScrollArea(self)
        scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        scroll.wheelEvent = empty_func
        scroll.setWidgetResizable(True)

        widget = QtWidgets.QWidget()
        hbox = QtWidgets.QHBoxLayout()
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(Image(self, self.im_size))
        layout.addStretch(1)

        hbox.addStretch()
        hbox.addLayout(layout)
        hbox.addStretch()

        widget.setLayout(hbox)
        scroll.setWidget(widget)

        self.setCentralWidget(scroll)
        self.resize(*self.wn_size)
        self.show()

    def update_image(self, ndarr, image_name=None):
        image = self.centralWidget().widget().layout()\
            .itemAt(1).layout().itemAt(0).widget()
        image.update_image(ndarr, image_name)
